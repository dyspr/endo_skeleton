var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize

var video
var poseNet
var pose
var skeleton
var videoAspectRatio
var scaleRatio

var poseHistory = []
var historyLength = 13

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  video = createCapture(VIDEO)
  video.hide()
  poseNet = ml5.poseNet(video, modelLoaded)
  poseNet.on('pose', gotPoses)
  videoAspectRatio = video.width / video.height
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  if (pose) {
    push()
    translate((windowWidth + video.width) * 0.5, (windowHeight - video.height) * 0.5)
    scale(-1, 1)
    let earR = pose.rightEar
    let earL = pose.leftEar
    let d = dist(earR.x, earR.y, earL.x, earL.y)
    noFill()
    stroke(255)
    strokeWeight(boardSize * 0.01)
    ellipse(pose.nose.x, pose.nose.y, d)

    noFill()
    stroke(255)
    strokeWeight(boardSize * 0.01)
    // neck
    line(pose.leftShoulder.x + (pose.rightShoulder.x - pose.leftShoulder.x) * 0.5, pose.leftShoulder.y + (pose.rightShoulder.y - pose.leftShoulder.y) * 0.5, pose.nose.x, pose.nose.y + d * 0.5)
    // body
    line(pose.leftShoulder.x, pose.leftShoulder.y, pose.rightShoulder.x, pose.rightShoulder.y)
    line(pose.leftHip.x, pose.leftHip.y, pose.rightHip.x, pose.rightHip.y)
    line(pose.leftShoulder.x, pose.leftShoulder.y, pose.leftHip.x, pose.leftHip.y)
    line(pose.rightShoulder.x, pose.rightShoulder.y, pose.rightHip.x, pose.rightHip.y)
    // right arm
    line(pose.rightShoulder.x, pose.rightShoulder.y, pose.rightElbow.x, pose.rightElbow.y)
    line(pose.rightElbow.x, pose.rightElbow.y, pose.rightWrist.x, pose.rightWrist.y)
    // left arm
    line(pose.leftShoulder.x, pose.leftShoulder.y, pose.leftElbow.x, pose.leftElbow.y)
    line(pose.leftElbow.x, pose.leftElbow.y, pose.leftWrist.x, pose.leftWrist.y)
    // right leg
    line(pose.rightHip.x, pose.rightHip.y, pose.rightKnee.x, pose.rightKnee.y)
    line(pose.rightKnee.x, pose.rightKnee.y, pose.rightAnkle.x, pose.rightAnkle.y)
    // left leg
    line(pose.leftHip.x, pose.leftHip.y, pose.leftKnee.x, pose.leftKnee.y)
    line(pose.leftKnee.x, pose.leftKnee.y, pose.leftAnkle.x, pose.leftAnkle.y)
    pop()

    poseHistory.push(pose)
    if (poseHistory.length > historyLength) {
      poseHistory = poseHistory.splice(1)
    }

    for (var i = 0; i < poseHistory.length; i++) {
      push()
      translate((windowWidth + video.width) * 0.5, (windowHeight - video.height) * 0.5)
      scale(-1, 1)
      let earR = poseHistory[i].rightEar
      let earL = poseHistory[i].leftEar
      let d = dist(earR.x, earR.y, earL.x, earL.y)
      noFill()
      stroke(255 * (i / poseHistory.length))
      strokeWeight(boardSize * 0.01)
      ellipse(poseHistory[i].nose.x, poseHistory[i].nose.y, d)

      noFill()
      stroke(255 * (i / poseHistory.length))
      strokeWeight(boardSize * 0.01)
      // neck
      line(poseHistory[i].leftShoulder.x + (poseHistory[i].rightShoulder.x - poseHistory[i].leftShoulder.x) * 0.5, poseHistory[i].leftShoulder.y + (poseHistory[i].rightShoulder.y - poseHistory[i].leftShoulder.y) * 0.5, poseHistory[i].nose.x, poseHistory[i].nose.y + d * 0.5)
      // body
      line(poseHistory[i].leftShoulder.x, poseHistory[i].leftShoulder.y, poseHistory[i].rightShoulder.x, poseHistory[i].rightShoulder.y)
      line(poseHistory[i].leftHip.x, poseHistory[i].leftHip.y, poseHistory[i].rightHip.x, poseHistory[i].rightHip.y)
      line(poseHistory[i].leftShoulder.x, poseHistory[i].leftShoulder.y, poseHistory[i].leftHip.x, poseHistory[i].leftHip.y)
      line(poseHistory[i].rightShoulder.x, poseHistory[i].rightShoulder.y, poseHistory[i].rightHip.x, poseHistory[i].rightHip.y)
      // right arm
      line(poseHistory[i].rightShoulder.x, poseHistory[i].rightShoulder.y, poseHistory[i].rightElbow.x, poseHistory[i].rightElbow.y)
      line(poseHistory[i].rightElbow.x, poseHistory[i].rightElbow.y, poseHistory[i].rightWrist.x, poseHistory[i].rightWrist.y)
      // left arm
      line(poseHistory[i].leftShoulder.x, poseHistory[i].leftShoulder.y, poseHistory[i].leftElbow.x, poseHistory[i].leftElbow.y)
      line(poseHistory[i].leftElbow.x, poseHistory[i].leftElbow.y, poseHistory[i].leftWrist.x, poseHistory[i].leftWrist.y)
      // right leg
      line(poseHistory[i].rightHip.x, poseHistory[i].rightHip.y, poseHistory[i].rightKnee.x, poseHistory[i].rightKnee.y)
      line(poseHistory[i].rightKnee.x, poseHistory[i].rightKnee.y, poseHistory[i].rightAnkle.x, poseHistory[i].rightAnkle.y)
      // left leg
      line(poseHistory[i].leftHip.x, poseHistory[i].leftHip.y, poseHistory[i].leftKnee.x, poseHistory[i].leftKnee.y)
      line(poseHistory[i].leftKnee.x, poseHistory[i].leftKnee.y, poseHistory[i].leftAnkle.x, poseHistory[i].leftAnkle.y)
      pop()
    }
  }
}

function vidLoad() {
  video.loop()
  video.volume(0)
}

function mousePressed() {
  video.loop()
  video.volume(0)
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function gotPoses(poses) {
  if (poses.length > 0) {
    pose = poses[0].pose
    skeleton = poses[0].skeleton
  }
}

function modelLoaded() {
  console.log('poseNet ready')
}
